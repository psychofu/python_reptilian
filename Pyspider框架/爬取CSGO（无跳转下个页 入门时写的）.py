#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# Created on 2020-03-10 21:01:24
# Project: Csgo_ZBT

from pyspider.libs.base_handler import *


class Handler(BaseHandler):
    crawl_config = {
    }

    @every(minutes=24 * 60)
    def on_start(self):
        self.crawl('https://www.zbt.com/cn/steam/csgo.html', callback=self.index_page,validate_cert=False)

    @config(age=10 * 24 * 60 * 60)
    def index_page(self, response):
        for each in response.doc('.col-10-2 .market-item').items():
            self.crawl(each.attr.href, callback=self.detail_page,validate_cert=False)
        

    @config(priority=2)
    def detail_page(self, response):
        return {
            "url": response.url,
            "title": response.doc('.media-body .f20').text(),
            '售价':response.doc('.ml10').text(),
            '类型':response.doc('li > .mr10').text(),
            
            
        }
